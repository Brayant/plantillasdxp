<style>
    .container.lead{
        display: block;
        max-width: unset;
        text-align: center;
        width: 100%;
    }
    .lead {
        font-size: 16px;
        font-weight: 300;
        line-height: 1.4;
        margin-bottom: 20px;
    }
    .lead .lightbox-modal{
        max-height: 780px;
    }
    .lead-form{
        display: inline-grid;
    }
    .lead-form .lead-submit{
        border: none; 
        height: 45px;
        padding: 0;
        vertical-align: middle; 
        width: 228px; 
        z-index: 1; 
    }
    .input-lead {
        background-color: #FFFFFF;
        border: 1px solid transparent;
        height: 44px;
        padding: 6px;
        vertical-align: middle;
        width: 228px;
        z-index: 1;
    }
    .lightbox-modal-call-nocturnal .input-lead {
        margin: 10px 0;
    }
    .lightbox-modal-call-nocturnal .input-lead.error {
        margin: 0px 0;
    }
    .lead-submit input{
        height: 100%;
        width: 100%;
    }
    .lead-submit input:invalid{
        border: 1px solid red !important;
    }
    .lightbox-modal-call-nocturnal .lead-email, .lightbox-modal-call-nocturnal .lead-phone, .lightbox-modal-call-nocturnal .lead-name, .lightbox-modal-call-nocturnal .lead-schedule, .lightbox-modal-call-nocturnal .lead-horary{   
        width: 100%;
    }
    .lead-email .error, .lead-phone .error,.lead-name .error, .lead-schedule .error, .lead-horary .error{     
        color: #ff6e6e;
        display: block;        
        font-family: 'Telefonica', sans-serif;
        font-weight: 500;
        padding: 10px;
        text-align: center;
        width: 228px;
    }
    .lead-acepto {
        padding-top: 25px;
    }
    .lead-acepto label.error{
        bottom: 0px;
        color: #ff6e6e;
        display: inline-block !important;
        font-family: 'Telefonica', sans-serif;
        font-weight: 500;
        position: absolute;
        
    }
    .input-lead.error {
        border: 1px solid #f1bcbc;
    }
    .input-lead:focus {
        background: #f8f8f8;
        box-shadow: none !important;
        outline: none;
    }   
    .titulo {
        background-color: transparent;
        color: #50535a !important;
        font-size: 40px;
        font-weight: lighter;
        line-height: 60px;
        margin: 0 auto;
        max-width: 1280px;
        overflow: hidden;
        padding-bottom: 20px;
        padding-top: 40px;
        text-align: center;
        text-overflow: ellipsis;
        white-space: nowrap;
        width: 70%;
    }
    @media screen and (max-width: 544px){
        .lead-phone, .input-lead, .lead-btn-llameme, .lead-submit {
            position: relative;
            width:100% !important;
        }
        .lead-email .error, .lead-phone .error,.lead-name .error{    
            width: 100%;
        }
    }
    @media screen and (max-width: 1025px) {
        .titulo {
            font-size: 20px;
        }
    }
    .lead .lightbox-modal {
        display: block;
        margin: 0 auto;
        height: max-content;
    }
    .lead-date {
        text-align: center;
        border-bottom: 1px solid black !important;
        box-shadow: none !important;
        border: none;
        padding: 16px 0px;
        text-align: center;
        display: inline-block !important;
    }
    .lead-date.error {
        border: 1px solid #f1bcbc !important;
    }
    .lead .horario2 {
        width: 100%;
        background: transparent;
        border: none;
    }
    .lightbox-modal-call-nocturnal .lead-acepto{
        margin: 20px 0;
        position: relative;
        padding: 26px 0;
    }
    input.lead-date::-webkit-inner-spin-button, input.lead-date::-webkit-clear-button {
        display: none;
    }
</style>
<#if entries?has_content>
	<#list entries as currentLead>   
        
        <div class="container lead">
         <#if isOutOfDate>
            <!-- MODAL NOCTURNO -->
            
            <div id="${ns}lightbox-call" class="lightbox-modal modal modal-large lightbox-modal-call-nocturnal" style="display: none;">
                <a href="#!" title="acción cerrar modal" class="lightbox-modal-close modal-action modal-close waves-effect waves-green btn-flat"></a> 
                <div class="lightbox-modal-content">
                    <div class="lightbox-modal__logo">
                        <svg class="lightbox-modal__logo--svg" height="73.8px" id="" version="1.1" viewBox="0 0 43 32" width="70px" x="0px" y="0px">
                            <path d="M43,16.118c-0.033,2.244-0.186,4.597-0.71,6.912c-0.158,0.699-0.491,1.311-0.966,1.824 c-0.617,0.661-1.392,0.966-2.293,0.743c-0.895-0.224-1.349-0.89-1.54-1.742c-0.262-1.163-0.043-2.332,0.077-3.489 .18-1.758,0.333-3.517,0.256-5.286c-0.038-0.928-0.153-1.873-0.841-2.566c-0.922-0.928-2.2-0.813-3.314,0.229 c-1.016,0.945-1.698,2.119-2.266,3.375c-1.146,2.544-1.922,5.22-2.746,7.873c-0.448,1.431-0.95,2.839-1.96,3.98 c-3.521,3.964-10.041,3.533-12.957-0.939c-1.627-2.495-3.014-5.138-4.515-7.715C8.897,18.761,8.547,18.22,8.1,17.745 c-0.486-0.508-1.026-0.743-1.736-0.563c-0.748,0.191-1.01,0.765-1.136,1.42c-0.186,0.994-0.054,1.977,0.142,2.959 c0.426,2.151,1.256,4.188,1.78,6.306c0.164,0.655,0.268,1.316,0.224,1.993c-0.055,0.775-0.377,1.415-1.108,1.72 C5.43,31.936,4.6,31.848,3.879,31.264c-0.699-0.568-1.081-1.371-1.387-2.179C0.92,24.969-0.2,20.753,0.03,16.293 c0.153-2.927,0.671-5.782,2.037-8.414c1.108-2.146,2.692-3.751,5.22-4.127c1.72-0.257,3.249,0.158,4.581,1.294 c1.556,1.327,2.643,3.019,3.756,4.695c0.841,1.256,1.518,2.626,2.506,3.778c0.95,1.114,2.085,1.949,3.631,1.895 c1.72-0.06,2.773-1.092,3.434-2.577c1.146-2.588,2.086-5.263,3.287-7.835c0.923-1.982,2.26-3.593,4.297-4.455 c3.058-1.294,5.853-0.251,7.497,2.741c1.343,2.452,1.999,5.127,2.38,7.879C42.875,12.771,42.957,14.382,43,16.118z" id="logo-movistar_3"></path>
                        </svg>
                    </div>
                    <div class="lightbox-modal__title">Solicita una llamada</div>
                    <div class="lightbox-modal__subtitle">Déjanos tu teléfono y un especialista te contactará para asesorarte.</div>
                    <div class="container lead">
                        <form class="lead-form row" name="${ns}leadForm" method="POST" onSubmit="event.preventDefault()"> 
                            <div class="col">
                                <#if getterUtil.getBoolean(currentLead.getHasName())>
                                    <div class="lead-name"> 
                                        <label class="hide" for="clientName">name</label>
                                        <input id="clientName" class="input-lead" name="${ns}name" pattern="[a-zA-Z\-'\s]+" placeholder="Introduce tu nombre" type="text"/> 
                                        
                                    </div>
                                </#if>
                                <#if getterUtil.getBoolean(currentLead.getHasEmail())>
                                    <div class="lead-email">
                                        <label class="hide" for="email">email</label>
                                        <input id="email" class="input-lead" name="${ns}email" placeholder="Introduce tu correo electrónico" type="text"/> 
                                        
                                    </div>
                                </#if>
                                <#if getterUtil.getBoolean(currentLead.getHasPhone())>
                                    <div class="lead-phone"> 
                                        <label class="hide" for="phone">Phone</label>
                                        <input id="phone" class="input-lead" name="${ns}phone" placeholder="Introduce tu número de celular" type="text"/> 
                                    </div>
                                </#if>
                                <div class="lead-schedule">
                                    <label class="hide active" for="fecha2">Fecha</label> 
                                    <input id="fecha2" class="input-lead lead-date" name="${ns}schedule" placeholder="Seleccione una fecha" type="date"/>
                                </div>   
                                <div class="lead-horary">
                                    <label class="hide" for="${ns}horary">Horario</label> 
                                    <select id="${ns}horary" class="input-lead" name="${ns}horary">
                                        <option disabled="disabled" selected="selected" value="">Seleccione el horario</option>
                                        <option value="10:00:00">entre las 9 a.m y 12 p.m</option>
                                        <option value="13:00:00">entre las 12 p.m y 3 p.m</option>
                                        <option value="16:00:00">entre las 3 p.m y 6 p.m</option>
                                        <option value="19:00:00">entre las 6 p.m y 9 p.m</option>
                                    </select>
                                </div>
                                <div class="lead-acepto">
                                    <label class="hide" for="TyC">acepto</label>
                                    <input class="lead-checkbox" name="TyC" type="checkbox" title="Debes aceptar el Aviso de Privacidad"/>
                                    <span class="lead-aviso" for="TyC">Acepto el <a href="/informacion-para-clientes/aviso-de-privacidad" style="color:#00a9e0" target="_blank"> Aviso de Privacidad </a></span>
                                </div>
                                <div class="lead-btn-llameme">
                                 <input class="input-lead" name="${ns}cookieId" type="hidden" value="" id="cook">
                                    <input class="input-lead" name="${ns}url" type="hidden" value="" id="urlAll">
                                    <label class="hide" for="Wunder-Cta-ModalNocturno">Enviar</label>
                                    <input id="Wunder-Cta-ModalNocturno" type="submit" class="bTNLlamenme btn waves-effect button-green hero__item-call-to-action lead-submit" value="Enviar"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> 
            <#else>
            <div id="${ns}lightbox-call" class="lightbox-modal modal modal-large lightbox-modal-call-nocturnal" style="display: none;">
                <a href="#!" title="acción cerrar modal" class="lightbox-modal-close modal-action modal-close waves-effect waves-green btn-flat"></a> 
                <div class="lightbox-modal-content">
                    <div class="lightbox-modal__logo">
                        <svg class="lightbox-modal__logo--svg" height="73.8px" id="" version="1.1" viewBox="0 0 43 32" width="70px" x="0px" y="0px">
                            <path d="M43,16.118c-0.033,2.244-0.186,4.597-0.71,6.912c-0.158,0.699-0.491,1.311-0.966,1.824 c-0.617,0.661-1.392,0.966-2.293,0.743c-0.895-0.224-1.349-0.89-1.54-1.742c-0.262-1.163-0.043-2.332,0.077-3.489 .18-1.758,0.333-3.517,0.256-5.286c-0.038-0.928-0.153-1.873-0.841-2.566c-0.922-0.928-2.2-0.813-3.314,0.229 c-1.016,0.945-1.698,2.119-2.266,3.375c-1.146,2.544-1.922,5.22-2.746,7.873c-0.448,1.431-0.95,2.839-1.96,3.98 c-3.521,3.964-10.041,3.533-12.957-0.939c-1.627-2.495-3.014-5.138-4.515-7.715C8.897,18.761,8.547,18.22,8.1,17.745 c-0.486-0.508-1.026-0.743-1.736-0.563c-0.748,0.191-1.01,0.765-1.136,1.42c-0.186,0.994-0.054,1.977,0.142,2.959 c0.426,2.151,1.256,4.188,1.78,6.306c0.164,0.655,0.268,1.316,0.224,1.993c-0.055,0.775-0.377,1.415-1.108,1.72 C5.43,31.936,4.6,31.848,3.879,31.264c-0.699-0.568-1.081-1.371-1.387-2.179C0.92,24.969-0.2,20.753,0.03,16.293 c0.153-2.927,0.671-5.782,2.037-8.414c1.108-2.146,2.692-3.751,5.22-4.127c1.72-0.257,3.249,0.158,4.581,1.294 c1.556,1.327,2.643,3.019,3.756,4.695c0.841,1.256,1.518,2.626,2.506,3.778c0.95,1.114,2.085,1.949,3.631,1.895 c1.72-0.06,2.773-1.092,3.434-2.577c1.146-2.588,2.086-5.263,3.287-7.835c0.923-1.982,2.26-3.593,4.297-4.455 c3.058-1.294,5.853-0.251,7.497,2.741c1.343,2.452,1.999,5.127,2.38,7.879C42.875,12.771,42.957,14.382,43,16.118z" id="logo-movistar_3"></path>
                        </svg>
                    </div>
                    <div class="lightbox-modal__title">${title}</div>
                    <div class="lightbox-modal__subtitle">${subtitle}</div>
                    <div class="container lead">
                        <form class="lead-form row" name="${ns}leadForm" method="POST" onSubmit="event.preventDefault()"> 
                            <div class="col">
                                <#if getterUtil.getBoolean(currentLead.getHasName())>
                                    <div class="lead-name"> 
                                        <label class="hide" for="clientName">name</label>
                                        <input id="clientName" class="input-lead" name="${ns}name" pattern="[a-zA-Z\-'\s]+" placeholder="Introduce tu nombre" type="text"/> 
                                        
                                    </div>
                                </#if>
                                <#if getterUtil.getBoolean(currentLead.getHasEmail())>
                                    <div class="lead-email">
                                        <label class="hide" for="email">email</label>
                                        <input id="email" class="input-lead" name="${ns}email" placeholder="Introduce tu correo electrónico" type="text"/> 
                                        
                                    </div>
                                </#if>
                                <#if getterUtil.getBoolean(currentLead.getHasPhone())>
                                    <div class="lead-phone"> 
                                        <label class="hide" for="phone">Phone</label>
                                        <input id="phone" class="input-lead" name="${ns}phone" placeholder="Introduce tu número de celular" type="text"/> 
                                    </div>
                                </#if>
                                <#if getterUtil.getBoolean(currentLead.getExtra1())>
                                    <div class="lead-name col col-md-12 col-sm-12 col-xs-12">
                                       <label class="hide" for="empresa">Nombre de equipo</label>
                                       <input name="${ns}extra1" wtrackname="equipo" wtrackparams="" wtracktype="formLead" type="text" pattern="[a-zA-Z\-'\s]+" class="input-lead col col-md-12 col-sm-12 col-xs-12 input1 " id="extra1" placeholder="Nombre de equipo">
                                    </div>
                                    </#if>
                                     <#if getterUtil.getBoolean(currentLead.getExtra2())>
                                    <div class="lead-name col col-md-12 col-sm-12 col-xs-12">
                                       <label class="hide" for="empresa">MRT</label>
                                       <input name="${ns}extra2" wtrackname="mrt" wtrackparams="" wtracktype="formLead" type="text" pattern="[a-zA-Z\-'\s]+" class="input-lead col col-md-12 col-sm-12 col-xs-12 input1 " id="extra2" placeholder="MRT">
                                    </div>
                                    </#if>
                                <div class="lead-acepto">
                                    <label class="hide" for="TyC">acepto</label>
                                    <input class="lead-checkbox" name="TyC" type="checkbox" title="Debes aceptar el Aviso de Privacidad"/>
                                    <span class="lead-aviso" for="TyC">Acepto el <a href="/informacion-para-clientes/aviso-de-privacidad" style="color:#00a9e0" target="_blank"> Aviso de Privacidad </a></span>
                                </div>
                                <div class="lead-btn-llameme">
                                 <input class="input-lead" name="${ns}cookieId" type="hidden" value="" id="cook">
                                    <input class="input-lead" name="${ns}url" type="hidden" value="" id="urlAll">
                                    <label class="hide" for="Wunder-Cta-Modal">Enviar</label>
                                    <input id="Wunder-Cta-Modal" type="submit" class="bTNLlamenme btn waves-effect button-green hero__item-call-to-action lead-submit" value="Llámame"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </#if> 
            <!-- MODAL SUCCESS -->
            <div id="${ns}lightbox-success" class="lightbox-modal modal modal-large" style="display: none;">
                <a href="#!" title="acción cerrar modal" class="lightbox-modal-close modal-action modal-close waves-effect waves-green btn-flat"></a> 
                <div class="lightbox-modal-content">
                    <div id="${ns}logo" class="lightbox-modal__logo">
                        
                    </div>
                    <div id="${ns}message" class="lightbox-modal__subtitle"></div>
                    <div class="container lead">
                        <div class="lead-btn-llameme">
                            <a title="acción aceptar modal" class="bTNLlamenme btn waves-effect button-green hero__item-call-to-action lead-submit modal-close">OK</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="${ns}logoTelefonica" style="display:none;">   
            <svg height="73.8px" width="70px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 426.667 426.667" style="enable-background:new 0 0 426.667 426.667;" xml:space="preserve">
                <path style="fill:#6AC259;" d="M213.333,0C95.518,0,0,95.514,0,213.333s95.518,213.333,213.333,213.333
                    c117.828,0,213.333-95.514,213.333-213.333S331.157,0,213.333,0z M174.199,322.918l-93.935-93.931l31.309-31.309l62.626,62.622
                    l140.894-140.898l31.309,31.309L174.199,322.918z"/>

            </svg>
        </div>

        <div id="${ns}logoError" style="display:none;">   
            <svg height="73.8px" width="70px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 286.054 286.054" style="enable-background:new 0 0 286.054 286.054;" xml:space="preserve">
                <path style="fill:#E2574C;" d="M143.027,0C64.04,0,0,64.04,0,143.027c0,78.996,64.04,143.027,143.027,143.027
                    c78.996,0,143.027-64.022,143.027-143.027C286.054,64.04,222.022,0,143.027,0z M143.027,259.236
                    c-64.183,0-116.209-52.026-116.209-116.209S78.844,26.818,143.027,26.818s116.209,52.026,116.209,116.209
                    S207.21,259.236,143.027,259.236z M143.036,62.726c-10.244,0-17.995,5.346-17.995,13.981v79.201c0,8.644,7.75,13.972,17.995,13.972
                    c9.994,0,17.995-5.551,17.995-13.972V76.707C161.03,68.277,153.03,62.726,143.036,62.726z M143.036,187.723
                    c-9.842,0-17.852,8.01-17.852,17.86c0,9.833,8.01,17.843,17.852,17.843s17.843-8.01,17.843-17.843
                    C160.878,195.732,152.878,187.723,143.036,187.723z"/>
            </svg>
        </div>  

        <script>

            $(document).ready(function(){
            var cookieID = document.cookie;
                if(cookieID == null || cookieID == ''){
                    
                        var onlyID = "Cookie no identificada";
                    }else{
                        var regex = /_ga=[^;]+/g;
                        if(cookieID.match(regex)){
                            var sessionID = cookieID.match(regex).toString();
                            var onlyID = sessionID.replace("_ga=","");  
                        }else{
                            var onlyID = "Cookie no identificada";
                        }
                    
                    }
                var pathname = window.location;
                $('input#cook').val(onlyID);
                $('input#urlAll').val(pathname);
                var modalCall = $('#${ns}lightbox-call');
                var modalSuccess = $('#${ns}lightbox-success');

                $('.${ns}modal').click(function(){
                    $(modalCall).modal('open');
                })

                $("form[name='${ns}leadForm']").validate({
                    rules: {
                            ${ns}email: {
                                required: true,
                                email: true
                            },
                            ${ns}name: {
                                required: true
                            },
                            ${ns}phone: {
                                required: true,
                                number: true,
                                minlength: 10,
                                maxlength: 10
                            },
                            ${ns}horary:{
                                required: true
                            },
                            ${ns}schedule:{
                                required:true
                            },
                            TyC: {
                                required: true
                            },
                            ${ns}extra1: {
                                required: true
                            },
                            ${ns}extra2: {
                                required: true
                            }
                        },
                        messages: {
                            ${ns}email: "Introduce un correo electrónico válido",
                            ${ns}phone: "Introduce un teléfono valido",
                            ${ns}name: "Introduce tu nombre",
                            ${ns}schedule: "Introduce una fecha valida.",
                            ${ns}horary: "Introduce un horario valido.",
                            ${ns}extra1: "Introduce un equipo valido.",
                            ${ns}extra2: "Introduce un dato valido."

                        },
                        submitHandler: function(form) {
                            $.ajax({
                                type: "POST",
                                dataType: 'json',
                                url: '${addScoringLeadResourceURL}',
                                data: $(form).serialize(), 
                                success: function(data){
                                    $('#${ns}message').text('');
                                    if(data.success){
                                        $(modalCall).modal('close');
                                        $('#${ns}message').text('Tu petición ha sido enviada, un asesor se comunicará contigo');
                                        $('#${ns}logo').html($('#${ns}logoTelefonica').html())
                                        $(modalSuccess).modal('open');
                                        $(form).trigger("reset");
                                    }else{
                                        $('#${ns}logo').html($('#${ns}logoError').html())
                                        switch(data.errorCode) {
                                            case 1:
                                               $('#${ns}message').text('La información ingresada es incorrecta, favor de validar');
                                               $(modalSuccess).modal('open');
                                                break;
                                            case 2:
                                                $('#${ns}message').text('Fuera de horario de servicio, intente mas tarde');
                                                $(modalSuccess).modal('open');
                                                break;
                                            case 3:
                                                $('#${ns}message').text('Ocurrio un error al procesar su solicitud, intente mas tarde.');
                                                $(modalSuccess).modal('open');
                                                break;
                                            case 9:
                                                $('#${ns}message').text('Ocurrio un error al procesar su solicitud, intente mas tarde.');
                                                $(modalSuccess).modal('open');
                                                break;        
                                        }
                                            
                                    }
                                    
                                },
                                error: function(data){
                                    console.log("error")
                                }
                            });
                            
                        }
                });
                
            });

        </script>

	</#list>
</#if>